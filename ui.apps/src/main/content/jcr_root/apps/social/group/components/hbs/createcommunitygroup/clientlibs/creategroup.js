/*
 *
 * ADOBE CONFIDENTIAL
 * __________________
 *
 *  Copyright 2016 Adobe Systems Incorporated
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Adobe Systems Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Adobe Systems Incorporated and its
 * suppliers and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Adobe Systems Incorporated.
 */

(function(window, document, Granite, $, SCF) {
    "use strict";
    $(document).ready(function() {
        $.get(SCF.config.urlRoot + "/etc/community/templates/groups/reference.social.json", function(data) {
            var items = data.items,
                dropdownItems;
            items.sort(sort_by("title", function(a) {
                return a.toUpperCase();
            }));

            dropdownItems = _.map(items, function(item) {
                return {
                    content: {
                        textContent: item.title
                    },
                    value: item.id
                }
            });

            var select = $("#social-group-bluePrintSelect").get(0);
            var selected = false;
            dropdownItems.forEach(function(value, index) {
                if (!selected) {
                    value.selected = true;
                    selected = true;
                }
                select.items.add(value);
            });
        }, "json");
        var sort_by = function(field, primer) {
            var key = primer ?
                function(x) {
                    return primer(x[field]);
                } :
                function(x) {
                    return x[field];
                };
            return function(a, b) {
                return a = key(a), b = key(b), ((a > b) - (b > a));
            };
        };

        //get suffix if any
        var index = document.location.href.lastIndexOf(".html");
        if (index > 0) {
            var suffix = document.location.href.substring(index + 5);
            $("input.js-coral-pathbrowser-input").val(suffix);
        }

        // disable invite fields when Optinal Membership is selected
        $("#console-group-inviteUsers :input").prop("disabled", true);
        $("input[name=type]:radio").change(function(event) {
            if (this.value.toLowerCase() === "public") {
                $("#console-group-inviteUsers :input").attr("disabled", true);
                $("#console-group-inviteUsers .coral-TagList").empty();
            } else {
                $("#console-group-inviteUsers :input").attr("disabled", false);
            }

        });
        //disable group root from basic tab
        $('#scf-community_group_root :input').attr('disabled', true);

        $("#cq-social-create-group-submit").on("click", function() {
            if (!window.FormData) {
                $(this).closest("form").submit();
                $(this).prop("disabled", true);
            } else {
                $(this).closest("form")
                    .submit(function(e) {
                        var error = _.bind(function(jqxhr, text, error) {
                            var message;
                            if (jqxhr.status == 500) { //vs bugfix
                                var errorDetails = $CQ($CQ.parseHTML(jqxhr.responseText));
                                var code = errorDetails.find("#Status").text();
                                message = errorDetails.find("#Message").text();
                                if (_.isEmpty(code)) {
                                    code = "";
                                }

                                if (_.isEmpty(message)) {
                                    message = CQ.I18n.get("Server Error Please try again");
                                }
                                //alert.set("heading", CQ.I18n.get("Error"));
                                //alert.set("content", code + " " + message);
                            } else if (jqxhr.status == 409) {
                                message = CQ.I18n.get("Site with your selected language already exists");
                            } else if (jqxhr.status == 400) {
                                message = CQ.I18n.get("Illegal name, 'resources' is a reserved name and cannot be used for the name of site.");
                            }
                            // errorMessageGenerator(message);
                            return false;
                        }, this);
                        var success = _.bind(function(response) {
                            var redirect = this.baseURI.replace("/communities/createcommunitygroup.html", "/communities/communitygroups.html");
                            window.location.href = redirect;
                        }, this);

                        $.ajax({
                            url: SCF.config.urlRoot + $('input[name=siteRoot]').val() + ".social.json",
                            type: 'POST',
                            data: new FormData(this),
                            processData: false,
                            contentType: false,
                            "error": error,
                            "success": success
                        });
                        $("#cq-social-create-group-submit").prop("disabled", true);
                        e.preventDefault();
                    });
            }
        });
    });

})(window, document, Granite, Granite.$, SCF);
