(function($CQ, _, Backbone, SCF) {
    "use strict";

    var CreateResource = SCF.Components["social/journal/components/hbs/journal"].Model;
    var CreateResourceView = SCF.Components["social/journal/components/hbs/journal"].View;


    SCF.registerComponent('knowledgeportal/components/content/journal-kp', CreateResource, CreateResourceView);



})($CQ, _, Backbone, SCF);
