package org.niti.knowledgeportal.core.api;

import java.util.List;

import com.adobe.cq.social.scf.SocialComponent;

/**
 * A SocialComponent that provides a logical representation of a project. SocialComponents are logical representations
 * of resources that represent the business view of a resource. They are also used to convert a resource into JSON.
 */
public interface UserInterestComponent extends SocialComponent {

    public static final String PROJECT_RESOURCE_TYPE = "knowledgeportal/components/content/user_interest";

    /**
     * @return the title of the project
     */
    List<String> getUserInterests();

    /**
     * @return the title of the project
     */
    List<String> getAllInterestAreas();
    
}
