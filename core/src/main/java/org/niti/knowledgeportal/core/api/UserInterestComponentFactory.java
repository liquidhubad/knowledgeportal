package org.niti.knowledgeportal.core.api;

import java.util.List;

import javax.jcr.RepositoryException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.niti.knowledgeportal.core.impl.UserInterestComponentImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.social.scf.ClientUtilities;
import com.adobe.cq.social.scf.QueryRequestInfo;
import com.adobe.cq.social.scf.SocialComponent;
import com.adobe.cq.social.scf.SocialComponentFactory;
import com.adobe.cq.social.scf.core.AbstractSocialComponentFactory;
import com.adobe.cq.social.user.api.UserProfileComponentFactory;

public class UserInterestComponentFactory extends AbstractSocialComponentFactory implements SocialComponentFactory {

	 private static final Logger LOG = LoggerFactory.getLogger(UserProfileComponentFactory.class);
	  
	  public SocialComponent getSocialComponent(Resource resource)
	  {
	    try
	    {
	      return new UserInterestComponentImpl(resource, getClientUtilities(resource.getResourceResolver()));
	    }
	    catch (RepositoryException e)
	    {
	      LOG.error("Error creating UserInterest", e);
	    }
	    return null;
	  }
	  
	  public SocialComponent getSocialComponent(Resource resource, SlingHttpServletRequest request)
	  {
	    try
	    {
	      Object formForwarderValue = request.getAttribute("cq.form.editresources");
	      if (((formForwarderValue instanceof List)) && (((List)formForwarderValue).size() == 1))
	      {
	        Object item = ((List)formForwarderValue).get(0);
	        if ((item instanceof Resource))
	        {
	          Resource targetUser = (Resource)item;
	          if (targetUser != null) {
	            return new UserInterestComponentImpl(targetUser, getClientUtilities(request));
	          }
	        }
	      }
	      return new UserInterestComponentImpl(resource, getClientUtilities(request));
	    }
	    catch (RepositoryException e)
	    {
	      LOG.error("Error creating UserInterest", e);
	    }
	    return null;
	  }
	  
	  public SocialComponent getSocialComponent(Resource resource, ClientUtilities clientUtils, QueryRequestInfo requestInfo)
	  {
	    try
	    {
	      return new UserInterestComponentImpl(resource, clientUtils);
	    }
	    catch (RepositoryException e)
	    {
	      LOG.error("Error creating UserInterest", e);
	    }
	    return null;
	  }
	  
	  public String getSupportedResourceType()
	  {
	    return "knowledgeportal/components/content/user_interest";
	  }

}
