

package org.niti.knowledgeportal.core.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.niti.knowledgeportal.core.api.UserInterestComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.social.community.api.CommunityContext;
import com.adobe.cq.social.scf.ClientUtilities;
import com.adobe.cq.social.scf.core.BaseSocialComponent;
import com.adobe.cq.social.site.api.NavBar;
import com.adobe.granite.security.user.UserProperties;
import com.adobe.granite.security.user.UserPropertiesManager;

public class UserInterestComponentImpl extends BaseSocialComponent implements UserInterestComponent {

	private static final Set<String> RESERVED_PREFIXES = new HashSet();
	  
	  static
	  {
	    RESERVED_PREFIXES.add("jcr:");
	    RESERVED_PREFIXES.add("rep:");
	    RESERVED_PREFIXES.add("sling:");
	  }
	  
	  private static final Logger LOG = LoggerFactory.getLogger(UserInterestComponentImpl.class);
	  private static final String DEFAULT_AVATAR_PATH = "/etc/designs/default/images/social/avatar.png";
	  private static final String ACTION_ALL = "read,add_node,remove,set_property";
	  private final UserPropertiesManager upm;
	  private Resource profileResource;
	  private final UserProperties userProperties;
	  private final ResourceResolver resolver;
	  
	  private final CommunityContext context;
	  private final List<String> items;
	
	public UserInterestComponentImpl(Resource resource, ClientUtilities clientUtilities)
		    throws RepositoryException
		  {
		    super(resource, clientUtilities);
		    this.resolver = resource.getResourceResolver();
		    this.upm = ((UserPropertiesManager)this.resolver.adaptTo(UserPropertiesManager.class));
		    this.context = ((CommunityContext)resource.adaptTo(CommunityContext.class));
		    this.items = new ArrayList<String>();
		    
		    SlingHttpServletRequest request = this.clientUtils.getRequest();
		    String authId = null;
		    Resource propertyNode = null;
		    if (request == null)
		    {
		      authId = this.clientUtils.getAuthorizedUserId();
		    }
		    else
		    {
		      UserProperties checkProperties = (UserProperties)resource.adaptTo(UserProperties.class);
		      if (checkProperties != null)
		      {
		        authId = checkProperties.getAuthorizableID();
		      }
		      else if (StringUtils.isEmpty(request.getRequestPathInfo().getSuffix()))
		      {
		        authId = this.clientUtils.getAuthorizedUserId();
		      }
		      else
		      {
		        String authPath = request.getRequestPathInfo().getSuffix();
		        if (StringUtils.startsWith(authPath, "/social/authors/")) {
		          authId = authPath.substring("/social/authors/".length());
		        } else if (!StringUtils.isEmpty(authPath)) {
		          propertyNode = this.resolver.resolve(authPath);
		        }
		      }
		    }
		    if (this.upm != null)
		    {
		      if (!StringUtils.isEmpty(authId)) {
		        this.userProperties = this.upm.getUserProperties(authId, "interest");
		      } else if (propertyNode != null) {
		        this.userProperties = this.upm.getUserProperties((Node)propertyNode.adaptTo(Node.class));
		      } else {
		        this.userProperties = null;
		      }
		    }
		    else {
		      this.userProperties = null;
		    }
		    this.profileResource = getResource();
		    
		    initAllInterestAreas(resource);
		  }
	
	private void initAllInterestAreas(Resource resource)
	  {
	    Resource siteRoot = resource.getResourceResolver().getResource(this.context.getSitePagePath());
	    List<Resource> pages = new ArrayList();
	    pages.add(siteRoot);
	    for (Resource child : siteRoot.getChildren()) {
	      if (child.isResourceType("cq:Page")) {
	        pages.add(child);
	      }
	    }
	    for (final Resource page : pages)
	    {
	      final ValueMap props = (ValueMap)page.getChild("jcr:content").adaptTo(ValueMap.class);
	      boolean hidden = props.containsKey("hideInNav");
	      if (!hidden) {
	        this.items.add((String)props.get("navTitle", ""));
	      }
	    }
	  }
	public String getAuthorizableID()
	  {
	    return this.userProperties != null ? this.userProperties.getAuthorizableID() : null;
	  }
	public List <String> getUserInterests()
	  {
	    List<String> result = new ArrayList<String>();
	    try
	    {
	      if (this.userProperties != null)
	      {
	        String[] names = this.userProperties.getPropertyNames();
	        for (String name : names) {
	          
	            //String value = this.userProperties.getProperty(name);
	            result.add(name);
	     
	        }
	       
	      }
	    }
	    catch (RepositoryException e)
	    {
	      LOG.error("failed to get ProfileProperties for {}", getAuthorizableID());
	    }
	    return result;
	  }
	
	public List <String> getAllInterestAreas()
	  {
	    
	    return this.items;
	  }

}
