package org.niti.knowledgeportal.core.service;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import com.adobe.cq.social.journal.client.api.JournalCommentSocialComponentFactory;


@SuppressWarnings("deprecation")
@Component(label = "resourceFactory")
@Service
public class CreateResourceSocialComponentFactory extends JournalCommentSocialComponentFactory {

	@Override
	public String getSupportedResourceType() {
		
		return "knowledgeportal/components/content/journal-kp";
	}

}
